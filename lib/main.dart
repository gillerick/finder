import 'package:finder/src/services/finder.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:finder/src/views/profile.dart';
import 'package:finder/src/views/home.dart';
import 'package:finder/src/views/search.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(providers: [ChangeNotifierProvider(create: (_) => Search())],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Finder',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: MainMap(),
      ),
    );
  }
}

class MainMap extends StatefulWidget {
  @override
  _MainMapState createState() => _MainMapState();

}

class _MainMapState extends State<MainMap> {
  int _selectedIndex = 0;

  //List of App Views
  final List<Widget> _children = [
    HomePage(),
    SearchPage(),
    ProfilePage(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
    //Navigate to selected view
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (BuildContext context) => _children[_selectedIndex]));
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Finder'),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.search), onPressed: () {})
        ],
      ),
      body: Container(child: GoogleMap(initialCameraPosition: position)),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: 0, // this will be set when a new tab is tapped
        items: _items,
        // currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[800],
        onTap: _onItemTapped,
      ),
    );
  }
}

//Initial position: Sendy Offices
final CameraPosition position = CameraPosition(
  target: LatLng(-1.2999411, 36.7708429),
  zoom: 12,
);


final _items = [
  BottomNavigationBarItem(
    label: '',
    tooltip: 'Home',
    activeIcon: IconButton(
        icon: Icon(
          Icons.home,
          color: Colors.orange,
        )),
    icon: Icon(Icons.home_filled),
  ),
  BottomNavigationBarItem(
    label: '',
    tooltip: 'Find',
    activeIcon: IconButton(
        icon: Icon(
          Icons.search_sharp,
          color: Colors.orange,
        )),
    icon: Icon(Icons.search),
  ),
  BottomNavigationBarItem(
    label: '',
    tooltip: 'Profile',
    activeIcon: IconButton(
        icon: Icon(
          Icons.person_outline,
          color: Colors.orange,
        )),
    icon: Icon(Icons.person),
  ),
];


//Search term to provide user input
class Search with ChangeNotifier, DiagnosticableTreeMixin{
  String _searchTerm = "";

  String get searchTerm => _searchTerm;

  void change({String searchTerm}){
    _searchTerm = searchTerm;
    notifyListeners();
  }

}