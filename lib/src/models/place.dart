import 'geometry.dart';

class Place {
  final Geometry geometry;
  final String name;
  final String id;

  Place({this.geometry,this.name, this.id});

  factory Place.fromJson(Map<String,dynamic> json){
    return Place(
      geometry:  Geometry.fromJson(json['geometry']),
      name: json['name'],
      id: json['id'],
    );
  }
}