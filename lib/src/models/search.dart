class PlaceSearch {
  final String term;

  PlaceSearch({this.term});

  factory PlaceSearch.fromJson(Map<String,dynamic> json){
    return PlaceSearch(
        term: json['candidates'],
    );
  }
}

class Predictions {
  final String description;
  final String placeId;

  Predictions({this.description, this.placeId});

  factory Predictions.fromJson(Map<String,dynamic> json){
    return Predictions(
        description: json['description'],
        placeId: json['place_id']
    );
  }
}