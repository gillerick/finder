import 'package:finder/src/models/geometry.dart';
import 'package:finder/src/models/place.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:finder/main.dart';
import 'package:provider/provider.dart';
import 'package:finder/src/services/finder.dart';

class SearchPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    String searchTerm = context.read<Search>().searchTerm;
    FindPlace finder = new FindPlace();
    return Scaffold(
      appBar: AppBar(
        title: Text('Search Location'),
      ),
      body: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(13),
            child: TextField(
              onChanged: (val) => context.read<Search>().change(searchTerm: val),
              decoration: InputDecoration(
                  border: OutlineInputBorder(), hintText: "Find Place"),
            ),
          ),
          FloatingActionButton.extended(
            onPressed: () => {finder.fetchPlace(searchTerm)},
            label: Text("Search"),
            icon: const Icon(Icons.add),
          ),
          Expanded(
            child: ListView.builder(itemBuilder: (context, index) {
              return ListTile(
                  // title: Text('Hey'),
                  // subtitle: Text('There'),
                  // leading: Icon(Icons.pin_drop),
                  );
            }),
          ),
        ],
      ),
    );
  }
}
