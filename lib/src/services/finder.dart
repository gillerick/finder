import 'dart:convert';

import 'package:finder/src/models/search.dart';
import 'package:http/http.dart' as http;
import 'package:finder/src/models/place.dart';
import 'dart:convert' as convert;



class FindPlace{
  final key = 'AIzaSyBE_qgiZEQlaDJgy55-lksIA_29OVklZ4w';


  Future<List<Predictions>> getAutocomplete(String search) async {
    var url =
        'https://maps.googleapis.com/maps/api/place/autocomplete/json?input=$search&types=(cities)&key=$key';
    var response = await http.get(url);
    var json = convert.jsonDecode(response.body);
    var jsonResults = json['predictions'] as List;
    return jsonResults.map((place) => Predictions.fromJson(place)).toList();
  }

  Future<List<Place>> fetchPlace(String term) async {
    var url = Uri.parse('https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=$term&inputtype=textquery&fields=name,place_id,geometry&key=$key');
    final response = await http.get(url);

    if (response.statusCode == 200) {
      var json = jsonDecode(response.body);
      // var jsonResult =json['candidates'] as Map<String, dynamic>;
      // return Place.fromJson(jsonResult);
      // var json = convert.jsonDecode(response.body);
      var jsonResults = json['candidates'] as List;
      var list = jsonResults.map((place) => Place.fromJson(place)).toList();
      return list;

    } else {
      throw Exception('Failed to load places');
    }
  }

}
